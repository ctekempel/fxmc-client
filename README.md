This project is a demonstration of consuming the Sitecore FXM web service directly with the aim of facilitating multi channel analytics stories.

Notes:

*  at this stage it doesn't demonstrate the passing of custom data for events, goals, etc. This will be able to be done - I just haven't had time to demonstrate it yet. Take a look at the documentation for the FXM JavaScript API and run the same commends, note the request structure and then replicate
* at this stag it doesn't handle identifying a contact either. Preliminary thoughts on this are to use custom event data on a "login event" which will allow you to pass the identifier through. From there hook into the "tracking.triggerpageevent" pipeline to grab the identifier form the custom event data you pass through and perform the identify calls from there

I'm very open to pull requests to finish this off, so please feel free.

Another blog post that could be useful in conjunction to this POC is [http://reyrahadian.com/2016/07/26/sitecore-8-how-to-update-contact-through-fxm-api/](http://reyrahadian.com/2016/07/26/sitecore-8-how-to-update-contact-through-fxm-api/) In this post Rey talks about using custom event data to pass details (e.g. identifying a contact) through.