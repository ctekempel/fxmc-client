namespace FxmNonWebTracker
{
    using System;

    public class TrackingResult
    {
        public string ContactId { get; set; }
        public string SessionId { get; set; }
        public DateTime ContactExpires { get; set; }
        public string SessionPath { get; set; }
    }
}