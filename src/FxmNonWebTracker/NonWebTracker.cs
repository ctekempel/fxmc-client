﻿namespace FxmNonWebTracker
{
    using System;
    using System.Net.Http;
    using System.Web;

    using Newtonsoft.Json;

    public class NonWebTracker
    {
        private readonly string appIdentifier;

        private readonly string fxmServerHostName;

        private const string FxmEndPointPath = "sitecore/api/ssc/Beacon/Service/";

        public NonWebTracker(string fxmServerHostName, string appIdentifier)
        {
            this.fxmServerHostName = fxmServerHostName;
            this.appIdentifier = appIdentifier;
        }

        public TrackingResult TrackImpression(
            string viewIdentifier,
            string referrer = "",
            string sessionId = "",
            string contactId = "")
        {
            const string Action = "beacon/trackPageVisit";

            var timeInTicks = DateTime.Now.Ticks;
            var encodedViewIdentifier = HttpUtility.UrlEncode($"{this.appIdentifier}{viewIdentifier}");
            var encodedReferrer = HttpUtility.UrlEncode(referrer);
            var actionUrl =
                $"{this.fxmServerHostName}{FxmEndPointPath}{Action}/?contactId={contactId}&sessionId={sessionId}&page={encodedViewIdentifier}&referrer={encodedReferrer}&rt={timeInTicks}";

            var trackingObject = ExecuteAction(actionUrl);

            return trackingObject;
        }

        public TrackingResult TrackGoal(
            string goalId,
            string viewIdentifier,
            string sessionId,
            string contactId)
        {
            const string Action = "trackGoal";

            var timeInTicks = DateTime.Now.Ticks;
            var encodedViewIdentifier = HttpUtility.UrlEncode($"{this.appIdentifier}{viewIdentifier}");
            var encodedgoalId = HttpUtility.UrlEncode(goalId);
            var actionUrl =
                $"{this.fxmServerHostName}{FxmEndPointPath}{encodedgoalId}/{Action}/?contactId={contactId}&sessionId={sessionId}&page={encodedViewIdentifier}&rt={timeInTicks}";

            var trackingObject = ExecuteAction(actionUrl);

            return trackingObject;

        }

        public TrackingResult TrackOutcome(string outcomeId, string viewIdentifier, string sessionId, string contactId, string monetaryValue = "0")
        {
            const string Action = "trackOutcome";

            var timeInTicks = DateTime.Now.Ticks;
            var encodedViewIdentifier = HttpUtility.UrlEncode($"{this.appIdentifier}{viewIdentifier}");
            var encodedOutcomeId = HttpUtility.UrlEncode(outcomeId);
            var actionUrl =
                $"{this.fxmServerHostName}{FxmEndPointPath}{encodedOutcomeId}/{Action}/?contactId={contactId}&sessionId={sessionId}&page={encodedViewIdentifier}&rt={timeInTicks}&monetaryValue={monetaryValue}";

            var trackingObject = ExecuteAction(actionUrl);

            return trackingObject;
        }

        public TrackingResult TrackCampaign(string campaignId, string viewIdentifier, string sessionId, string contactId)
        {
            const string Action = "trackCampaign";

            var timeInTicks = DateTime.Now.Ticks;
            var encodedViewIdentifier = HttpUtility.UrlEncode($"{this.appIdentifier}{viewIdentifier}");
            var encodedCampaignId = HttpUtility.UrlEncode(campaignId);
            var actionUrl =
                $"{this.fxmServerHostName}{FxmEndPointPath}{encodedCampaignId}/{Action}/?contactId={contactId}&sessionId={sessionId}&page={encodedViewIdentifier}&rt={timeInTicks}";

            var trackingObject = ExecuteAction(actionUrl);

            return trackingObject;
        }

        public TrackingResult TrackEvent(
            string eventId,
            string viewIdentifier,
            string sessionId,
            string contactId)
        {
            const string Action = "trackEvent";

            var timeInTicks = DateTime.Now.Ticks;
            var encodedViewIdentifier = HttpUtility.UrlEncode($"{this.appIdentifier}{viewIdentifier}");
            var encodedEventId = HttpUtility.UrlEncode(eventId);
            var actionUrl =
                $"{this.fxmServerHostName}{FxmEndPointPath}{encodedEventId}/{Action}/?contactId={contactId}&sessionId={sessionId}&page={encodedViewIdentifier}&rt={timeInTicks}";

            var trackingObject = ExecuteAction(actionUrl);

            return trackingObject;
        }

        private static TrackingResult ExecuteAction(string actionUrl)
        {
            var task = new HttpClient().PostAsync(actionUrl, new StringContent(string.Empty));

            task.Wait();
            if (!task.Result.IsSuccessStatusCode)
            {
                throw new Exception(task.Result.ReasonPhrase);
            }

            var contentTask = task.Result.Content.ReadAsStringAsync();
            contentTask.Wait();
            var content = contentTask.Result;

            var trackingObject = JsonConvert.DeserializeObject<TrackingResult>(content);
            return trackingObject;
        }
    }
}