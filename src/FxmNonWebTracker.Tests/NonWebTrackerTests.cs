﻿namespace FxmNonWebTracker.Tests
{
    using System;

    using FluentAssertions;

    using NUnit.Framework;

    [TestFixture]
    public class NonWebTrackerTests
    {
        private const string FxmServerHostName = "http://sitecore.8.2.2.dev.net/";

        // Needs to match a hostname defined in FXM
        private const string AppIdentifier = "http://app.sitecore.8.2.2.dev.net/";

        [Test]
        public void ShouldTrackFirstViewImpression()
        {
            var subject = new NonWebTracker(
                FxmServerHostName,
                AppIdentifier);
            string viewIdentifier = "some-child-view";
            var result = subject.TrackImpression(viewIdentifier);
            result.Should().NotBeNull();

            result.ContactExpires.Should().BeAfter(DateTime.Now);
            result.ContactId.Should().NotBeNullOrEmpty();
            result.SessionId.Should().NotBeNullOrEmpty();
            result.SessionPath.Should().NotBeNullOrEmpty();
        }

        [Test]
        public void ShouldTrackSecondViewImpression()
        {
            var subject = new NonWebTracker(
                FxmServerHostName,
                AppIdentifier);

            var previousTracking = EstablishSession(subject);

            string secondViewIdentifier = "some-other-child-view";
            string referrer = string.Empty;
            var secondTrack = subject.TrackImpression(
                secondViewIdentifier,
                referrer,
                previousTracking.SessionId,
                previousTracking.ContactId);

            secondTrack.ContactExpires.Should().BeAfter(DateTime.Now);
            secondTrack.ContactId.Should().NotBeNullOrEmpty();
            secondTrack.SessionId.Should().NotBeNullOrEmpty();
            secondTrack.SessionPath.Should().NotBeNullOrEmpty();
        }

        private static TrackingResult EstablishSession(NonWebTracker subject)
        {
            string firstViewIdentifier = "some-child-view";
            var firstTrack = subject.TrackImpression(firstViewIdentifier);
            firstTrack.Should().NotBeNull();
            return firstTrack;
        }

        [Test]
        public void ShouldTrackGoal()
        {
            var subject = new NonWebTracker(
                FxmServerHostName,
                AppIdentifier);

            var previousTracking = EstablishSession(subject);


            string viewIdentifier = "some-child-view-that-triggers-a-goal";
            string goalId = "{968897F1-328A-489D-88E8-BE78F4370958}"; // Brochures request, ships with default install

            var result = subject.TrackGoal(goalId,viewIdentifier,previousTracking.SessionId,previousTracking.ContactId);
            result.Should().NotBeNull();

            result.ContactExpires.Should().BeAfter(DateTime.Now);
            result.ContactId.Should().NotBeNullOrEmpty();
            result.SessionId.Should().NotBeNullOrEmpty();
            result.SessionPath.Should().NotBeNullOrEmpty();
        }

        [Test]
        public void ShouldTriggerPageEvent()
        {
            var subject = new NonWebTracker(
                FxmServerHostName,
                AppIdentifier);

            var previousTracking = EstablishSession(subject);


            string viewIdentifier = "some-child-view-that-triggers-a-page-event";
            string eventId = "{FA72E131-3CFD-481C-8E15-04496E9586DC}"; // Download event, ships with default install

            var result = subject.TrackEvent(eventId, viewIdentifier, previousTracking.SessionId, previousTracking.ContactId);
            result.Should().NotBeNull();

            result.ContactExpires.Should().BeAfter(DateTime.Now);
            result.ContactId.Should().NotBeNullOrEmpty();
            result.SessionId.Should().NotBeNullOrEmpty();
            result.SessionPath.Should().NotBeNullOrEmpty();
        }

        [Test]
        public void ShouldTriggerCampaign()
        {
            var subject = new NonWebTracker(
                FxmServerHostName,
                AppIdentifier);

            var previousTracking = EstablishSession(subject);


            string viewIdentifier = "some-child-view-that-triggers-a-campaign";
            string campaignId = "{D195C517-C752-4CB5-9BF9-545C49409131}"; // Example Campaign, not sure if this ships or is one that I created

            var result = subject.TrackCampaign(campaignId, viewIdentifier, previousTracking.SessionId, previousTracking.ContactId);
            result.Should().NotBeNull();

            result.ContactExpires.Should().BeAfter(DateTime.Now);
            result.ContactId.Should().NotBeNullOrEmpty();
            result.SessionId.Should().NotBeNullOrEmpty();
            result.SessionPath.Should().NotBeNullOrEmpty();
        }

        [Test]
        public void ShouldTriggerOutcome()
        {
            var subject = new NonWebTracker(
                FxmServerHostName,
                AppIdentifier);

            var previousTracking = EstablishSession(subject);


            string viewIdentifier = "some-child-view-that-triggers-a-campaign";
            string outcomeId = "{BF6B8EE3-9FFB-4C58-9CB4-301C1C710F89}"; //Opportunity, shipos out of the box

            var result = subject.TrackOutcome(outcomeId, viewIdentifier, previousTracking.SessionId, previousTracking.ContactId);
            result.Should().NotBeNull();

            result.ContactExpires.Should().BeAfter(DateTime.Now);
            result.ContactId.Should().NotBeNullOrEmpty();
            result.SessionId.Should().NotBeNullOrEmpty();
            result.SessionPath.Should().NotBeNullOrEmpty();
        }

        [Test]
        public void ShouldTriggerOutcomeWithMonetaryValue()
        {
            var subject = new NonWebTracker(
                FxmServerHostName,
                AppIdentifier);

            var previousTracking = EstablishSession(subject);


            string viewIdentifier = "some-child-view-that-triggers-a-campaign";
            string outcomeId = "{BF6B8EE3-9FFB-4C58-9CB4-301C1C710F89}"; //Opportunity, shipos out of the box

            var result = subject.TrackOutcome(outcomeId, viewIdentifier, previousTracking.SessionId, previousTracking.ContactId,"1000000");
            result.Should().NotBeNull();

            result.ContactExpires.Should().BeAfter(DateTime.Now);
            result.ContactId.Should().NotBeNullOrEmpty();
            result.SessionId.Should().NotBeNullOrEmpty();
            result.SessionPath.Should().NotBeNullOrEmpty();
        }

    }
}